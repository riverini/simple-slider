<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>test</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<div class="slider">
    <div class="slider__card img" style="background-color: red"> 01</div>
    <div class="slider__card img" style="background-color: blue"> 02</div>
    <div class="slider__card img" style="background-color: green"> 03</div>
    <div class="slider__card img" style="background-color: yellow"> 04</div>
</div>
<div class="dots"></div>
</body>
</html>
<script src="slider.js"></script>
